import matplotlib.pyplot as plt
import numpy as np

delta = 1

x = y = np.arange(-3.0, 3.01, delta)
X, Y = np.meshgrid(x, y)
Z = X + Y

fig2, ax2 = plt.subplots(layout="constrained")
levels = [-1.5, -1, -0.5, 0, 0.5, 1]
CS3 = ax2.contourf(X, Y, Z, colors=("r", "g", "b"), extend="both")

# CS4 = ax2.contour(X, Y, Z, levels, colors=('k',), linewidths=(3,))
# ax2.set_title('Listed colors (3 masked regions)')
# ax2.clabel(CS4, fmt='%2.1f', colors='w', fontsize=14)

# Notice that the colorbar gets all the information it
# needs from the ContourSet object, CS3.
fig2.colorbar(CS3)
plt.show()
