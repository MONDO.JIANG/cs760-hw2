import numpy as np
from scipy.interpolate import lagrange
import matplotlib.pyplot as plt
from numpy.polynomial.polynomial import Polynomial
import random
from sklearn.metrics import mean_squared_error, mean_squared_log_error
import math


def main():
    a = 0
    b = 25
    n = 100
    mean = 0
    sd_mutiplier = 3
    # standard deviation for gaussian
    sds = np.arange(a, b * sd_mutiplier, (b - a) / 10)
    X = np.random.uniform(low=a, high=b, size=n)
    X_test = np.random.uniform(low=a, high=b, size=n)
    Y_actual = np.sin(X_test)

    figure_w = 2
    figure_h = math.ceil(len(sds) / figure_w)
    fig = plt.figure(figsize=(figure_h, figure_w))
    fig.subplots_adjust(hspace=0.5, left=0.02, bottom = 0.03, right=0.98, top=0.97)
    plots = [
        fig.add_subplot(figure_h, figure_w, i + 1)
        for i in range(figure_w * figure_h)
    ]
    X_grid = np.linspace(a, b, n * 3)

    losses = np.empty(len(sds))
    for idx, sd in enumerate(sds):
        noise = np.random.normal(mean, sd, size=X.shape)
        X_n = X + noise
        Y = np.sin(X_n)
        poly = lagrange(X_n, Y)

        Y_predict = poly(X_test)
        loss = mean_squared_error(Y_actual, Y_predict)
        losses[idx] = np.log2(loss)
        print(f"sd: {sd}, MSE: {loss}, log MSE: {np.log2(loss)}")

        plot = plots[idx]
        plot.set_ylim(min(Y) - 2, max(Y) + 2)
        plot.scatter(X, Y, label="data")
        plot.plot(X_grid, poly(X_grid), label="Polynomial")
        plot.set_title(
            f"sd: {sd}, log(MSE): {np.log2(loss):.2f} "
        )
        plot.label_outer()

    plots[0].legend()

    fig_trend = plt.figure()
    plot_trend = fig_trend.add_subplot()
    plot_trend.set_xlabel("sd")
    plot_trend.set_ylabel("log(MSE)")
    plot_trend.plot(sds, losses, label="log(MSE)")
    plot_trend.legend()

    plt.show()


def single_run(a, b, n):
    Y = np.sin(X)
    poly = lagrange(X, Y)
    X_new = np.sort(np.random.uniform(low=a, high=b, size=n))
    Y_actual = np.sin(X_new)
    Y_predict = poly(X_new)
    loss = mean_squared_error(Y_actual, Y_predict)
    print(f"MSE: {loss}, log MSE: {np.log2(loss)}")

    plt.figure(1)
    plt.scatter(X, Y, label="data")
    plt.legend()

    X_grid = np.linspace(a, b, n * 3)
    plt.figure(2)
    plt.ylim(min(Y) - 2, max(Y) + 2)
    plt.scatter(X, Y, label="data")
    plt.plot(X_grid, poly(X_grid), label="Polynomial")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
