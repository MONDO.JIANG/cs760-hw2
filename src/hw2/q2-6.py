from anytree import RenderTree, NodeMixin
from hw2.decision_tree import DecisionTree, stringfy_data, print_tree
from matplotlib import pyplot as plt
import numpy as np
import math


def main():
    d1 = DecisionTree()
    d1.load_txt("data/D1.txt")
    d2 = DecisionTree()
    d2.load_txt("data/D2.txt")

    d1.split()
    d2.split()

    plot_step = 0.01
    x1min = d1.data[:, 0].min()
    x1max = d1.data[:, 0].max()
    x2min = d1.data[:, 1].min()
    x2max = d1.data[:, 1].max()
    x1range = np.arange(math.floor(x1min), math.ceil(x1max), plot_step)
    x2range = np.arange(math.floor(x2min), math.ceil(x2max), plot_step)
    xx1, xx2 = np.meshgrid(x1range, x2range)
    y1 = np.empty(xx1.shape)
    y2 = np.empty(xx1.shape)
    for idx1, x1 in enumerate(x1range):
        for idx2, x2 in enumerate(x2range):
            # idx need to reverse to match, see:
            # https://www.geeksforgeeks.org/numpy-meshgrid-function/
            # print(xx1[idx2, idx1], x1)
            # print(xx2[idx2, idx1], x2)
            y1[idx2, idx1] = d1.predict(x1, x2)
            y2[idx2, idx1] = d2.predict(x1, x2)
    levels = [0, 1]

    plot1 = plt.subplot(1, 2, 1)
    plot1.axes.set(
        xlim=(x1min, x1max),
        ylim=(x2min, x2max),
    )
    plot1.set_title("D1.txt")
    plot1.contourf(xx1, xx2, y1, extends="both", cmap="Set3")
    plot1.contour(xx1, xx2, y1, levels=levels, colors="r", linewidths=2)
    plot1.scatter(d1.data_y_0[:, 0], d1.data_y_0[:, 1], marker="o")
    plot1.scatter(d1.data_y_1[:, 0], d1.data_y_1[:, 1], marker="x")

    plot2 = plt.subplot(1, 2, 2)
    plot2.axes.set(
        xlim=(x1min, x1max),
        ylim=(x2min, x2max),
    )
    plot2.set_title("D2.txt")
    plot2.contourf(xx1, xx2, y2, extends="both", cmap="Set3")
    plot2.contour(xx1, xx2, y2, levels=levels, colors="r", linewidths=2)
    plot2.scatter(
        d2.data_y_0[:, 0],
        d2.data_y_0[:, 1],
        marker="o",
    )
    plot2.scatter(
        d2.data_y_1[:, 0],
        d2.data_y_1[:, 1],
        marker="x",
    )

    plt.show()


if __name__ == "__main__":
    main()
