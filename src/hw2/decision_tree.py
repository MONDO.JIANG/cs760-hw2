from anytree import RenderTree, NodeMixin
from anytree.exporter import UniqueDotExporter
from typing import TypeAlias, Literal, Self, Type, Any
from enum import Enum
import numpy as np
from itertools import groupby
from functools import cached_property

DEBUG = True

# target can only be 1 or 0
TargetType: TypeAlias = Literal[0, 1]
# each line we get from the data is a tuple of (x_1, x_2, y)
DataItem: TypeAlias = tuple[float, float, TargetType]
Data: TypeAlias = list[DataItem]
SplitOn: TypeAlias = Literal["x_1", "x_2"]
SplitDirection: TypeAlias = Literal["Left", "Right"]


class SplitCandidate:
    def __init__(
        self, parent: "DecisionTree", split_on: SplitOn, split_thereshold: float
    ):
        """split_on: j, split_thereshold: c"""
        self.parent = parent
        self.data = parent.data
        self.split_on = split_on
        self.split_thereshold = split_thereshold

    @cached_property
    def split_feature_index(self):
        return 0 if self.split_on == "x_1" else 1

    @cached_property
    def left_data(self):
        return self.data[
            self.data[:, self.split_feature_index] >= self.split_thereshold
        ]

    @cached_property
    def left_len(self):
        return len(self.left_data)

    @cached_property
    def right_data(self):
        return self.data[self.data[:, self.split_feature_index] < self.split_thereshold]

    @cached_property
    def right_len(self):
        return len(self.right_data)

    @cached_property
    def split_info(self):
        return calculate_entropy_by_distri([self.left_len, self.right_len])

    @cached_property
    def left_node(self):
        return DecisionTree(data=self.left_data, split_directin="Left")

    @cached_property
    def right_node(self):
        return DecisionTree(data=self.right_data, split_directin="Right")

    @cached_property
    def weighted_entropy(self):
        total = len(self.data)
        return np.multiply(
            np.divide([self.left_len, self.right_len], total),
            [self.left_node.entropy, self.right_node.entropy],
        ).sum()

    @cached_property
    def info_gain(self):
        return self.parent.entropy - self.weighted_entropy

    @cached_property
    def gain_ratio(self):
        return self.info_gain / self.split_info


class DecisionTree(NodeMixin):
    DecisionTreeType: TypeAlias = "DecisionTree"

    xs: np.ndarray
    ys: np.ndarray
    is_inited: bool = False
    parent: DecisionTreeType
    is_leaf: bool = False
    label: TargetType
    split_direction: SplitDirection

    _data: Data
    _split_instance: SplitCandidate
    _left: DecisionTreeType
    _right: DecisionTreeType

    def __init__(
        self: Self,
        data: Data = None,
        parent: DecisionTreeType = None,
        left: DecisionTreeType = None,
        right: DecisionTreeType = None,
        split_directin: SplitDirection = None,
        **kwargs,
    ):
        self.__dict__.update(kwargs)
        if data is not None:
            self.data = data
        self.parent = parent
        self.left = left
        self.right = right
        self.split_direction = split_directin

    def load_txt(self, path: str):
        self.data = np.loadtxt(fname=path, delimiter=" ")

    @property
    def data(self) -> Data:
        return self._data

    @data.setter
    def data(self, value: Data):
        self._data = value
        self.is_inited = True

    @property
    def left(self) -> DecisionTreeType:
        return self._left

    @left.setter
    def left(self, value: DecisionTreeType):
        if value is None:
            return
        self._left = value
        value.parent = self

    @property
    def right(self) -> DecisionTreeType:
        return self._right

    @right.setter
    def right(self, value: DecisionTreeType):
        if value is None:
            return
        self._right = value
        value.parent = self

    @property
    def split_instance(self) -> SplitCandidate:
        return self._split_instance

    @split_instance.setter
    def split_instance(self, value):
        self._split_instance = value
        self.left = value.left_node
        self.right = value.right_node

    @cached_property
    def xs(self):
        return np.array(self.data[:, :-1])

    @cached_property
    def ys(self):
        return np.array(self.data[:, -1])

    @cached_property
    def data_y_0(self):
        return self.data[self.data[:, 2] == 0]

    @cached_property
    def data_y_1(self):
        return self.data[self.data[:, 2] == 1]

    @cached_property
    def entropy(self):
        # Assertion: target only has 0 or 1
        y0s = self.data_y_0
        y1s = self.data_y_1
        y0len = len(y0s)
        y1len = len(y1s)
        return calculate_entropy_by_distri([y0len, y1len])

    def stop_and_compute_label(self):
        y0len = len(self.data_y_0)
        y1len = len(self.data_y_1)
        self.is_leaf = True

        if y0len > y1len:
            self.label = 0
            return
        # equal and 1 is the majority, label is 1
        self.label = 1

    def split(self):
        if not self.is_inited:
            raise Exception("DecisionTree not inited")

        # stopping criteria 1: the node is empty
        if len(self.data) == 0 or self.data is None:
            self.stop_and_compute_label()
            return

        # debug("[split]", xs=self.xs, ys=self.ys)
        splits = self.candidate_splits

        if DEBUG:
            for s in splits:
                print(
                    f"""parent_entropy: {self.entropy}, split_on: {s.split_on}, split_thereshold: {s.split_thereshold}
        left_node_e: {s.left_node.entropy}, right_node_e: {s.right_node.entropy}
        weighted_entropy: {s.weighted_entropy}
        info_gain: {s.info_gain}
        split_info: {s.split_info}
        gain_ratio: {s.gain_ratio}
    """
                )
        # skip candidate with zero split info
        # TODO: what the hell is "the entropy of the split? branch or weighted entropy?"
        # We shouldn't stop when the entropy of the split is zero!
        filtered_splits = [s for s in splits if s.gain_ratio > 0]
        # stopping criteria 3: the entropy of any candidates split is zero
        if len(filtered_splits) == 0:
            self.stop_and_compute_label()
            return
        # ignore tie, just pick the one max returns
        best_split = max(splits, key=lambda s: s.gain_ratio)
        # biggest gain ratio is 0, then all splits have 0 gain ratio
        # stopping criteria 2: all splits have zero gain ratio
        if best_split.gain_ratio == 0:
            self.stop_and_compute_label()
            return

        self.split_instance = best_split
        self.left.split()
        self.right.split()

    @cached_property
    def candidate_splits(self) -> list[SplitCandidate]:
        uniq_x1s = np.unique(self.xs[:, 0])
        uniq_x2s = np.unique(self.xs[:, 1])

        candidate_splits: list[SplitCandidate] = [
            SplitCandidate(self, "x_1", x1) for x1 in uniq_x1s
        ] + [SplitCandidate(self, "x_2", x2) for x2 in uniq_x2s]
        # filter splits that are not "split"
        candidate_splits = [s for s in candidate_splits if s.left_len and s.right_len]
        return candidate_splits

    def predict(self, x_1, x_2):
        if self.is_leaf:
            return self.label
        if self.split_instance is None:
            raise Exception("predict error: split instance is none")

        v = x_1 if self.split_instance.split_on == "x_1" else x_2
        if v >= self.split_instance.split_thereshold:
            return self.left.predict(x_1, x_2)
        else:
            return self.right.predict(x_1, x_2)

    @property
    def size(self):
        if self.is_leaf:
            return 1
        return self.left.size + self.right.size + 1


def calculate_entropy_by_distri(distribution: [float, float]):
    total = np.sum(distribution)
    if total == 0:
        return 0
    ps = np.divide(distribution, total)
    # Assertion: when p = 0, p * log2(p) = 0
    ps = list(filter(lambda x: x != 0, ps))
    entropy = -(ps * np.log2(ps)).sum()
    return entropy


def stringfy_data(data: Data):
    return " | ".join([",".join(map(str, t)) for t in data])


def print_tree(tree: DecisionTree, show_data=False):
    for pre, fill, node in RenderTree(tree):
        branch_direction = (
            ""
            if node.split_direction is None
            else ("then: " if node.split_direction == "Left" else "else: ")
        )
        data = f", data: {stringfy_data(node.data)}" if show_data else ""
        entropy = f", entropy: {abs(node.entropy)}"
        if node.is_leaf:
            print(f"{pre}{branch_direction}label: {node.label}{entropy}{data}")
            continue
        weighted_split_entropy = (
            f", weighted_split_entropy: {node.split_instance.weighted_entropy}"
        )
        print(
            f"{pre}{branch_direction}{node.split_instance.split_on} >= {node.split_instance.split_thereshold}{entropy}{weighted_split_entropy}{data}"
        )


def export_tree(tree: DecisionTree, path: str):
    UniqueDotExporter(
        tree, nodeattrfunc=nodeattrfunc, edgeattrfunc=edgeattrfunc
    ).to_picture(path)


def edgeattrfunc(node: DecisionTree, child: DecisionTree):
    branch_direction = (
        ""
        if child.split_direction is None
        else ("then" if child.split_direction == "Left" else "else")
    )
    return f'label="{branch_direction}"'


def nodeattrfunc(node: DecisionTree):
    if node.is_leaf:
        return f'label="{node.label}"'
    return f'label="{node.split_instance.split_on} >= {node.split_instance.split_thereshold}"'


def debug(*args, **kwargs):
    if DEBUG:
        print("debug:", *args, kwargs)
