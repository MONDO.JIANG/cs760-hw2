from anytree import RenderTree, NodeMixin
from hw2.decision_tree import DecisionTree, stringfy_data, print_tree, Data, DataItem
from matplotlib import pyplot as plt
import numpy as np
import math
from functools import reduce


def main():
    data = np.loadtxt(fname="data/Dbig.txt", delimiter=" ")
    permuted = np.random.permutation(data)
    test_data = permuted[8192:]
    train_sizes = [32, 128, 512, 2048, 8192]
    # train_sizes = [32, 128]
    train_sets = [permuted[0:size] for size in train_sizes]
    dts = [train_dt(s) for s in train_sets]
    train_errors = [compute_test_err(dt, test_data) for dt in dts]
    fig1 = plt.figure(figsize=(3, 2))
    plots = [
        fig1.add_subplot(3, 2, 1),
        fig1.add_subplot(3, 2, 2),
        fig1.add_subplot(3, 2, 3),
        fig1.add_subplot(3, 2, 4),
        fig1.add_subplot(3, 2, 5),
    ]
    for idx, t in enumerate(train_sizes):
        dt = dts[idx]
        plot = plots[idx]
        print_tree(dt)
        plot.set_title(
            f"List {t}, number node: {dts[idx].size}, err_n: {round(train_errors[idx], 2)}"
        )
        plot_decision_boundary(plot, dt)

    for p in plots:
        p.label_outer()

    plt.figure(2)
    plt.plot(train_sizes, train_errors)
    plt.suptitle("learning curve")

    plt.show()


def train_dt(data: Data):
    dt = DecisionTree(data=data)
    dt.split()
    return dt


def compute_test_err(dt: DecisionTree, test_data: Data):
    # tcount, fcount
    tcount, fcount = reduce(
        lambda res, item: (res[0] + 1, res[1])
        if test_item(dt, item)
        else (res[0], res[1] + 1),
        test_data,
        (0, 0),
    )
    assert tcount + fcount == len(test_data)
    return fcount / (tcount + fcount)


def test_item(dt: DecisionTree, item: DataItem):
    prediction = dt.predict(item[0], item[1])
    return item[2] == prediction


def plot_decision_boundary(plot, dt: DecisionTree, plot_step=0.01):
    x1min = dt.data[:, 0].min()
    x1max = dt.data[:, 0].max()
    x2min = dt.data[:, 1].min()
    x2max = dt.data[:, 1].max()
    x1range = np.arange(math.floor(x1min), math.ceil(x1max), plot_step)
    x2range = np.arange(math.floor(x2min), math.ceil(x2max), plot_step)
    xx1, xx2 = np.meshgrid(x1range, x2range)
    y = np.empty(xx1.shape)
    for idx1, x1 in enumerate(x1range):
        for idx2, x2 in enumerate(x2range):
            y[idx2, idx1] = dt.predict(x1, x2)
    plot.axes.set(
        xlim=(x1min, x1max),
        ylim=(x2min, x2max),
    )
    plot.contourf(xx1, xx2, y, extends="both", cmap="Set3")
    plot.scatter(dt.data_y_0[:, 0], dt.data_y_0[:, 1], marker="o")
    plot.scatter(dt.data_y_1[:, 0], dt.data_y_1[:, 1], marker="x")


if __name__ == "__main__":
    main()
