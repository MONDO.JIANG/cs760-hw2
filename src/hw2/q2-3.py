from anytree import RenderTree, NodeMixin
from hw2.decision_tree import DecisionTree, stringfy_data, print_tree


def main():
    root = DecisionTree()
    root.load_txt("data/Druns.txt")
    for s in root.candidate_splits:
        print(f"""{s.split_on}>= {s.split_thereshold}
        entropy: {s.weighted_entropy}
        info_gain: {s.info_gain}
        split_info: {s.split_info}
        gain_ratio: {s.gain_ratio}
        """)

if __name__ == "__main__":
    main()
