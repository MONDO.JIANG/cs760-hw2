from anytree import RenderTree, NodeMixin
from hw2.decision_tree import DecisionTree, stringfy_data, print_tree
from matplotlib import pyplot as plt


def main():
    root = DecisionTree()
    root.load_txt("data/q2-2.txt")
    root.split()
    print_tree(root)
    plt.scatter(root.data_y_0[:, 0], root.data_y_0[:, 1], marker="o")
    plt.scatter(root.data_y_1[:, 0], root.data_y_1[:, 1], marker="x")
    # for d in root.data:
    #     print(d)
    #     plt.plot(d[0], d[1], marker="o" if d[2] == 0 else "x", color="r" if d[2] == 0 else "b")
    plt.show()


if __name__ == "__main__":
    main()
