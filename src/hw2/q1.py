from anytree import RenderTree, NodeMixin
from hw2.decision_tree import DecisionTree, stringfy_data


def main():
    root = DecisionTree()
    root.load_txt("data/Druns.txt")
    root.split()

    for pre, fill, node in RenderTree(root):
        if node.is_leaf:
            print(
                f"{pre} {'then' if node.split_direction == 'Left' else 'else'} label: {node.label}, entropy: {node.entropy}, data: {stringfy_data(node.data)}"
            )
            continue
        print(
            f"{pre}{node.split_instance.split_on} < {node.split_instance.split_thereshold}, data: {stringfy_data(node.data)}"
        )


if __name__ == "__main__":
    main()
