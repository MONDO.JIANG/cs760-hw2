from anytree import RenderTree, NodeMixin
from hw2.decision_tree import DecisionTree, stringfy_data, print_tree, export_tree


def main():
    root = DecisionTree()
    root.load_txt("data/D1.txt")
    root.split()
    export_tree(root, "img/q2-5-1-dot.png")


if __name__ == "__main__":
    main()
