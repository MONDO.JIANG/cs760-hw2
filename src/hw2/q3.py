from hw2.decision_tree import DecisionTree, stringfy_data, print_tree, Data, DataItem
from matplotlib import pyplot as plt
import numpy as np
from functools import reduce
from sklearn import tree
from sklearn.metrics import zero_one_loss


def main():
    data = np.loadtxt(fname="data/Dbig.txt", delimiter=" ")
    permuted = np.random.permutation(data)
    test_data = permuted[8192:]
    train_sizes = [32, 128, 512, 2048, 8192]
    # train_sizes = [32, 128]
    train_sets = [permuted[0:size] for size in train_sizes]
    dts = [train_dt(s) for s in train_sets]
    train_errors = [compute_test_err(dt, test_data) for dt in dts]


    for idx, dt in enumerate(dts):
        print(
            f"D_{train_sizes[idx]}, number of nodes: {dt.tree_.node_count}, err_n: {train_errors[idx]}"
        )

    plt.plot(train_sizes, train_errors)
    plt.suptitle("learning curve")

    plt.show()


def train_dt(data: Data):
    dt = tree.DecisionTreeClassifier()
    X = data[:, 0:2]
    Y = data[:, 2].flat
    dt.fit(X, Y)
    return dt


def compute_test_err(dt: tree.DecisionTreeClassifier, test_data: Data):
    X = test_data[:, 0:2]
    Y_actual = test_data[:, 2].flat
    Y_predict = dt.predict(X)
    return zero_one_loss(Y_actual, Y_predict)


def test_item(dt: DecisionTree, item: DataItem):
    prediction = dt.predict([item[0:2]])
    return item[2] == prediction


if __name__ == "__main__":
    main()
