from anytree import RenderTree, NodeMixin
from hw2.decision_tree import DecisionTree, stringfy_data, print_tree, export_tree


def main():
    root = DecisionTree()
    root.load_txt("data/D3leaves.txt")
    root.split()
    # print_tree(root)
    export_tree(root, "img/q2-4-dot.png")


if __name__ == "__main__":
    main()
